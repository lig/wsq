package wsq

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/nsqio/go-nsq"
)

var msgSep = []byte{32}
var msgErrTag = []byte("#err")

type wsConnI interface {
	Close() error
	NextWriter(messageType int) (io.WriteCloser, error)
	ReadMessage() (messageType int, p []byte, err error)
	SetWriteDeadline(t time.Time) error
}

var p nsq.Producer

type nsqProducerI interface {
	Ping() error
	Publish(topic string, body []byte) error
	Stop()
}

type topicMessage[M any] struct {
	topic   string
	message M
}

type errorMessage struct {
	code        int
	description string
}

type client[M any] struct {
	id           string
	wsConn       wsConnI
	user         User
	wsqConfig    *Config
	nsqConfig    *nsq.Config
	transformer  *Transformer[M]
	nsqProducer  nsqProducerI
	pumpsWG      sync.WaitGroup
	wsWriteChan  chan topicMessage[M]
	nsqConsumers map[string]*nsq.Consumer
}

func newClient[M any](
	wsConn wsConnI,
	user User,
	wsqConfig *Config,
	nsqConfig *nsq.Config,
	transformer *Transformer[M],
) *client[M] {
	nsqProducer, err := nsq.NewProducer(wsqConfig.NSQDAddr, nsqConfig)
	if err != nil {
		log.Fatal(err)
	}
	return &client[M]{
		id:           uuid.Must(uuid.NewRandom()).String(),
		wsConn:       wsConn,
		user:         user,
		wsqConfig:    wsqConfig,
		nsqConfig:    nsqConfig,
		transformer:  transformer,
		nsqProducer:  nsqProducer,
		wsWriteChan:  make(chan topicMessage[M], 256),
		nsqConsumers: make(map[string]*nsq.Consumer),
	}
}

func marshalBypass(topic string, data []byte) ([]byte, error) { return data, nil }

func (c *client[M]) run() {
	c.pumpsWG.Add(2)

	go c.wsReadPump()
	go c.wsWritePump()

	c.pumpsWG.Wait()
}

func (c *client[M]) stop() {
	c.nsqProducer.Stop()
	c.wsConn.Close()
}

func (c *client[M]) wsReadPump() {
	defer func() {
		c.stop()
		c.pumpsWG.Done()
	}()

	err := c.nsqProducer.Ping()
	if err != nil {
		log.Fatal(err)
	}

	for {
		_, rawData, err := c.wsConn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Println(err)
			}
			break
		}

		data := rawData[:]
		cmd, cmdArgs, hasArgs := bytes.Cut(data, msgSep)
		switch strings.ToUpper(string(cmd)) {
		case "PUB":
			if !hasArgs {
				c.handleClientError("PUB usage: `PUB topic message`")
				continue
			}

			topic, wsMessage, hasMessage := bytes.Cut(cmdArgs, msgSep)
			if !hasMessage {
				c.handleClientError("PUB requires message")
				continue
			}

			topicName := string(topic)

			if !c.user.CanPub(topicName) {
				c.handleAccessError(fmt.Sprintf("Cannot publish to `%s`", topicName))
				continue
			}

			message, err := c.transformer.WSEnDec.UnMarshal(topicName, wsMessage)
			if err != nil {
				c.handleServerError("Cannot unmarshal message", err)
				break
			}

			nsqMessage, err := c.transformer.NSQEnDec.Marshal(topicName, message)
			if err != nil {
				c.handleServerError("Cannot marshal message", err)
				break
			}

			err = c.nsqProducer.Publish(topicName, nsqMessage)
			if err != nil {
				c.handleServerError(fmt.Sprintf("Cannot publish to NSQ topic `%s`", topic), err)
				break
			}
		case "SUB":
			if !hasArgs {
				c.handleClientError("SUB usage: `SUB topic`")
				continue
			}

			topic, _, hasExtra := bytes.Cut(cmdArgs, msgSep)
			if hasExtra {
				c.handleClientError("SUB requires topic name only")
				continue
			}

			topicName := string(topic)

			if !c.user.CanSub(topicName) {
				c.handleAccessError(fmt.Sprintf("Cannot subscribe to `%s`", topicName))
				continue
			}

			err = c.subscribe(topicName)
			if err != nil {
				c.handleServerError(fmt.Sprintf("Cannot subscribe to NSQ topic `%s`", topicName), err)
				break
			}
		case "USUB":
			if !hasArgs {
				c.handleClientError("USUB usage: `USUB topic`")
				continue
			}
			topic, _, hasExtra := bytes.Cut(cmdArgs, msgSep)
			if hasExtra {
				c.handleClientError("USUB requires topic name only")
				continue
			}
			err = c.unsubscribe(string(topic))
			if err != nil {
				c.handleServerError(fmt.Sprintf("Cannot unsubscribe from NSQ topic `%s`", topic), err)
				break
			}
		default:
			c.handleClientError(fmt.Sprintf("Unknown command `%s`", cmd))
			continue
		}
	}
}

func (c *client[M]) wsWritePump() {
	defer func() {
		c.stop()
		c.pumpsWG.Done()
	}()

	for topicMessage := range c.wsWriteChan {
		wsMessage, err := c.transformer.WSEnDec.Marshal(topicMessage.topic, topicMessage.message)
		if err != nil {
			log.Println(err)
			return
		}

		err = c.sendToWS(
			[]byte(topicMessage.topic),
			msgSep,
			wsMessage,
		)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

func (c *client[M]) subscribe(topic string) error {

	consumer, err := nsq.NewConsumer(
		topic,
		fmt.Sprintf("%s.%s#ephemeral", c.wsqConfig.NSQChannelPrefix, c.id),
		c.nsqConfig,
	)
	if err != nil {
		return err
	}

	consumer.AddHandler(&nsqMessageHandler[M]{
		user:        c.user,
		topic:       topic,
		wsWriteChan: c.wsWriteChan,
		nsqEnDec:    c.transformer.NSQEnDec,
	})

	if len(c.wsqConfig.NSQLookupdAddrs) == 0 {
		err = consumer.ConnectToNSQD(c.wsqConfig.NSQDAddr)
	} else {
		err = consumer.ConnectToNSQLookupds(c.wsqConfig.NSQLookupdAddrs)
	}
	if err != nil {
		return err
	}

	if prevConsumer, exists := c.nsqConsumers[topic]; exists {
		prevConsumer.Stop()
		delete(c.nsqConsumers, topic)
	}

	c.nsqConsumers[topic] = consumer
	return nil
}

func (c *client[M]) unsubscribe(topic string) error {
	consumer, exists := c.nsqConsumers[topic]
	if !exists {
		return fmt.Errorf("consumer not found for topic `%s`", topic)
	}

	consumer.Stop()
	delete(c.nsqConsumers, topic)

	return nil
}

func (c *client[M]) handleClientError(errDesc string) {
	log.Printf("Client error: %s\n", errDesc)
	c.sendErrorToWS(errorMessage{
		code:        400,
		description: errDesc,
	})
}

func (c *client[M]) handleAccessError(errDesc string) {
	log.Printf("Access error: %s\n", errDesc)
	c.sendErrorToWS(errorMessage{
		code:        403,
		description: errDesc,
	})
}

func (c *client[M]) handleServerError(errDesc string, err error) {
	log.Printf("%s: %s\n", errDesc, err)
	c.sendErrorToWS(errorMessage{
		code:        500,
		description: errDesc,
	})
}

func (c *client[M]) sendErrorToWS(errMsg errorMessage) error {
	return c.sendToWS(
		msgErrTag,
		msgSep,
		[]byte(fmt.Sprint(errMsg.code)),
		msgSep,
		[]byte(errMsg.description),
	)
}

func (c *client[M]) sendToWS(chunks ...[]byte) error {
	c.updateWSWriteDeadline()

	writer, err := c.wsConn.NextWriter(websocket.BinaryMessage)
	if err != nil {
		return err
	}

	for _, chunk := range chunks {
		_, err = writer.Write(chunk)
		if err != nil {
			return err
		}
	}

	if err := writer.Close(); err != nil {
		return err
	}

	return nil
}

func (c *client[M]) updateWSWriteDeadline() {
	var writeDeadline time.Time

	switch c.wsqConfig.WSWriteTimeout {
	case time.Duration(0):
		writeDeadline = time.Time{}
	default:
		writeDeadline = time.Now().Add(c.wsqConfig.WSWriteTimeout)
	}

	c.wsConn.SetWriteDeadline(writeDeadline)
}

type nsqMessageHandler[M any] struct {
	user        User
	nsqEnDec    EnDec[M]
	topic       string
	wsWriteChan chan topicMessage[M]
}

func (h *nsqMessageHandler[M]) HandleMessage(m *nsq.Message) error {
	if len(m.Body) == 0 {
		log.Printf("Skipping empty message in topic `%s`\n", h.topic)
		return nil
	}

	message, err := h.nsqEnDec.UnMarshal(h.topic, m.Body)
	if err != nil {
		log.Println(err)
		return err
	}

	if !h.user.CanRead(h.topic, message) {
		return nil
	}

	h.wsWriteChan <- topicMessage[M]{topic: h.topic, message: message}
	return nil
}
