package wsq

/*
Stores NSQ and Websocket [EnDec] interface implementations.

This is a generic struct. Its instance must be instantiated using type `M` which
coordinates Marshal method input and UnMarshal method output types across both EnDecs.
*/
type Transformer[M any] struct {
	NSQEnDec EnDec[M]
	WSEnDec  EnDec[M]
}

/*
Defines interface for handling serialization separately on each of NSQ and Websocket
sides.

This is a generic interface. The implementation must be instantiated using type `M`
which coordinates Marshal method input and UnMarshal method output types.
*/
type EnDec[M any] interface {
	Marshal(topic string, msg M) ([]byte, error)
	UnMarshal(topic string, data []byte) (M, error)
}

/*
An [EnDec] implementation that uses `[]byte` type as message representation and doesn't
perform any transformation on the data itself.
*/
type BypassEnDec struct{}

func (b BypassEnDec) Marshal(topic string, msg []byte) ([]byte, error) {
	return msg[:], nil
}

func (b BypassEnDec) UnMarshal(topic string, data []byte) ([]byte, error) {
	return data[:], nil
}

/*
A [Transformer] instance that uses [BypassEnDec] on both sides.
*/
var DummyTransformer = Transformer[[]byte]{
	NSQEnDec: &BypassEnDec{},
	WSEnDec:  &BypassEnDec{},
}
