package wsq

import (
	"net/http"
	"time"
)

/*
Config struct holds wsq-specific configuration values. Use [NewConfig] to build a new
instance and alter the configuration using setter methods as needed.

See [nsq.Config] docs for nsq-specific configuration.
*/
type Config struct {
	// Producer NSQD Address in the form "<host>:<port>". The default is "nsqd:4150".
	NSQDAddr string
	// A list of NSQ Lookupd addresses ("<host>:<port>") to be used for Consumer.
	// Uses `NSQDAddr` if `NSQLookupdAddrs` is empty (the default)
	NSQLookupdAddrs []string
	// The prefix to use for channel names being created by WSQ. The default is "WSQ".
	NSQChannelPrefix string
	// See gorilla/websocket docs for Upgrader: https://pkg.go.dev/github.com/gorilla/websocket@v1.5.0?utm_source=gopls#Upgrader.CheckOrigin
	// The default is `nil`.
	WSCheckOrigin func(r *http.Request) bool
	// Websocket write timeout. Zero duration means no timeout. The default is 10 seconds.
	WSWriteTimeout time.Duration
}

// NewConfig function creates a new wsq `Config` instance and returns its pointer.
func NewConfig() *Config {
	return &Config{
		NSQDAddr:         "nsqd:4150",
		NSQLookupdAddrs:  []string{},
		NSQChannelPrefix: "WSQ",
		WSCheckOrigin:    nil,
		WSWriteTimeout:   10 * time.Second,
	}
}

func (c *Config) SetNSQDAddr(value string) {
	c.NSQDAddr = value
}

func (c *Config) SetNSQLookupdAddrs(value []string) {
	c.NSQLookupdAddrs = value
}

func (c *Config) SetNSQChannelPrefix(value string) {
	c.NSQChannelPrefix = value
}

func (c *Config) SetWSCheckOrigin(value func(r *http.Request) bool) {
	c.WSCheckOrigin = value
}

func (c *Config) SetWSWriteTimeout(value time.Duration) {
	c.WSWriteTimeout = value
}
