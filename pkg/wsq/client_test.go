package wsq

import (
	"bytes"
	"fmt"
	"io"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/nsqio/go-nsq"
)

type fakeWSConn struct {
	messagesIn  chan string
	messagesOut chan string
}

func newFakeWSConn() *fakeWSConn {
	return &fakeWSConn{
		messagesIn:  make(chan string, 256),
		messagesOut: make(chan string, 256),
	}
}

func (f *fakeWSConn) Close() error {
	return nil
}

func (f *fakeWSConn) NextWriter(messageType int) (io.WriteCloser, error) {
	if messageType != websocket.BinaryMessage {
		panic("Unexpected message type")
	}
	return &fakeMessageWriter{messages: f.messagesOut}, nil
}

func (f *fakeWSConn) ReadMessage() (messageType int, p []byte, err error) {
	return websocket.BinaryMessage, []byte(<-f.messagesIn), nil
}

func (f *fakeWSConn) SetWriteDeadline(t time.Time) error {
	return nil
}

type fakeMessageWriter struct {
	data     bytes.Buffer
	messages chan string
}

func (f *fakeMessageWriter) Write(p []byte) (n int, err error) {
	f.data.Write(p)
	return len(p), nil
}

func (f *fakeMessageWriter) Close() error {
	f.messages <- f.data.String()
	f.data.Reset()
	return nil
}

type fakeNSQProducer struct {
	topicMessagesOut map[string]chan string
}

func newFakeNSQProducer() *fakeNSQProducer {
	return &fakeNSQProducer{
		topicMessagesOut: make(map[string]chan string),
	}
}

func (f *fakeNSQProducer) Ping() error {
	return nil
}

func (f *fakeNSQProducer) Publish(topic string, body []byte) error {
	if _, ok := f.topicMessagesOut[topic]; !ok {
		f.topicMessagesOut[topic] = make(chan string, 256)
	}
	f.topicMessagesOut[topic] <- string(body)
	return nil
}

func (f *fakeNSQProducer) Stop() {}

type testClientData[M any] struct {
	name        string
	user        User
	transformer *Transformer[M]
	nsqContent  string
	wsContent   string
}

type nsqEndDec[M []byte] struct{}

func (ed *nsqEndDec[M]) Marshal(topic string, msg M) ([]byte, error) {
	return []byte(fmt.Sprintf("nsq: %s", msg)), nil
}
func (ed *nsqEndDec[M]) UnMarshal(topic string, data []byte) (M, error) {
	return data[5:], nil
}

type wsEndDec[M []byte] struct{}

func (ed *wsEndDec[M]) Marshal(topic string, msg M) ([]byte, error) {
	return []byte(fmt.Sprintf("ws: %s", msg)), nil
}
func (ed *wsEndDec[M]) UnMarshal(topic string, data []byte) (M, error) {
	return data[4:], nil
}

var fakeTransformer = Transformer[[]byte]{
	NSQEnDec: &nsqEndDec[[]byte]{},
	WSEnDec:  &wsEndDec[[]byte]{},
}

type filteringUser struct{}

func (u *filteringUser) IsAuthenticated() bool {
	return true
}
func (u *filteringUser) CanPub(topic string) bool {
	return true
}
func (u *filteringUser) CanSub(topic string) bool {
	return true
}
func (u *filteringUser) CanRead(topic string, message any) bool {
	if string(message.([]byte)) == "FILTER ME" {
		return false
	}
	return true
}

func TestClient(t *testing.T) {
	const topicName = "some-topic"

	for _, testData := range []testClientData[[]byte]{
		{
			name:        "default/latin",
			user:        NewAnonymousUser(true),
			transformer: &DummyTransformer,
			nsqContent:  "message content",
			wsContent:   "message content",
		},
		{
			name:        "default/cyrillic",
			user:        NewAnonymousUser(true),
			transformer: &DummyTransformer,
			nsqContent:  "содержание сообщения",
			wsContent:   "содержание сообщения",
		},
		{
			name:        "serialization/latin",
			user:        NewAnonymousUser(true),
			transformer: &fakeTransformer,
			nsqContent:  "nsq: message content",
			wsContent:   "ws: message content",
		},
		{
			name:        "serialization/cyrillic",
			user:        NewAnonymousUser(true),
			transformer: &fakeTransformer,
			nsqContent:  "nsq: содержание сообщения",
			wsContent:   "ws: содержание сообщения",
		},
	} {
		conn := newFakeWSConn()
		client := newClient(
			conn,
			testData.user,
			NewConfig(),
			nsq.NewConfig(),
			testData.transformer,
		)
		nsqProducer := newFakeNSQProducer()
		client.nsqProducer = nsqProducer
		go client.run()
		t.Cleanup(client.stop)

		t.Run(fmt.Sprintf("%s/WSWritePump", testData.name), func(t *testing.T) {
			message, _ := testData.transformer.NSQEnDec.UnMarshal(topicName, []byte(testData.nsqContent))
			client.wsWriteChan <- topicMessage[[]byte]{topic: topicName, message: message}
			actualWSMessage := <-conn.messagesOut
			if actualWSMessage != fmt.Sprintf("%s %s", topicName, testData.wsContent) {
				t.Error(actualWSMessage)
			}
		})
		t.Run(fmt.Sprintf("%s/WSReadPump", testData.name), func(t *testing.T) {
			conn.messagesIn <- fmt.Sprintf("PUB %s %s", topicName, testData.wsContent)
			time.Sleep(100 * time.Millisecond)
			if _, ok := nsqProducer.topicMessagesOut[topicName]; !ok {
				t.Errorf("No topic `%s`", topicName)
				t.FailNow()
			}
			actualNSQMessage := <-nsqProducer.topicMessagesOut[topicName]
			if actualNSQMessage != testData.nsqContent {
				t.Error(actualNSQMessage)
			}
		})
	}
}

func TestNSQMessageHandler(t *testing.T) {
	const topicName = "some-topic"
	const noContent = "NO CONTENT"

	for _, testData := range []testClientData[[]byte]{
		{
			name:        "filtering/allow",
			user:        &filteringUser{},
			transformer: &DummyTransformer,
			nsqContent:  "message content",
			wsContent:   "message content",
		},
		{
			name:        "filtering/deny",
			user:        &filteringUser{},
			transformer: &DummyTransformer,
			nsqContent:  "FILTER ME",
			wsContent:   noContent,
		},
	} {
		t.Run(testData.name, func(t *testing.T) {
			wsChan := make(chan topicMessage[[]byte], 256)
			msgHandler := &nsqMessageHandler[[]byte]{
				user:        testData.user,
				nsqEnDec:    testData.transformer.NSQEnDec,
				topic:       topicName,
				wsWriteChan: wsChan,
			}

			msgHandler.HandleMessage(
				nsq.NewMessage(nsq.MessageID{}, []byte(testData.nsqContent)))

			select {
			case actualTopicMessage := <-wsChan:
				if string(actualTopicMessage.message) != testData.wsContent {
					t.Error(actualTopicMessage)
				}
			default:
				if testData.wsContent != noContent {
					t.Errorf("Expected to receive `%s` but didn't", testData.wsContent)
				}
			}
		})
	}
}
