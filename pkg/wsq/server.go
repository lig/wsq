package wsq

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/nsqio/go-nsq"
)

func CheckOriginBypass(r *http.Request) bool {
	return true
}

type Server[M any, U User] struct {
	addr          string
	wsqConfig     *Config
	nsqConfig     *nsq.Config
	upgrader      websocket.Upgrader
	clients       map[*client[M]]bool
	transformer   *Transformer[M]
	authenticator Authenticator[U]
}

/*
Creates new WSQ server using defaults for [Config], [nsq.Config], [Transformer], and
[Authenticator].
*/
func NewDefaultServer(addr string) *Server[[]byte, *AnonymousUser] {
	return NewServer[[]byte, *AnonymousUser](
		addr,
		NewConfig(),
		nsq.NewConfig(),
		&DummyTransformer,
		&NoAuthentication,
	)
}

/*
Creates new WSQ server.

This function is generic and must be instantiated using types `M` representing the type
for message data in between unmarshalling and marshalling (see [Transformer]) and `U`
representing [User] interface implementation (see [Authenticator]).
*/
func NewServer[M any, U User](
	addr string,
	wsqConfig *Config,
	nsqConfig *nsq.Config,
	transformer *Transformer[M],
	authenticator Authenticator[U],
) *Server[M, U] {
	return &Server[M, U]{
		addr:          addr,
		wsqConfig:     wsqConfig,
		nsqConfig:     nsqConfig,
		transformer:   transformer,
		authenticator: authenticator,
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin:     wsqConfig.WSCheckOrigin,
		},
		clients: make(map[*client[M]]bool),
	}
}

func (s *Server[M, U]) Run() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		s.serveClient(w, r)
	})
	err := http.ListenAndServe(s.addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func (s *Server[M, U]) serveClient(w http.ResponseWriter, r *http.Request) {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	user, err := s.authenticator.Authenticate(conn, r)
	if err != nil {
		log.Println(err)
		return
	}

	client := newClient(conn, user, s.wsqConfig, s.nsqConfig, s.transformer)
	s.clients[client] = true

	go client.run()
}
