module codeberg.org/lig/wsq/_demo/srv

go 1.18

require (
	codeberg.org/lig/wsq v0.2.1-0.20220714012657-84441b908b8a
	github.com/fxamacker/cbor v1.5.1
	github.com/nsqio/go-nsq v1.1.0
)

require (
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/x448/float16 v0.8.4 // indirect
)
