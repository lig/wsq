module codeberg.org/lig/wsq

go 1.18

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	github.com/nsqio/go-nsq v1.1.0
)

require github.com/golang/snappy v0.0.4 // indirect
